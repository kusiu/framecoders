<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProjectCreateRequest;
use App\Http\Requests\ProjectUpdateRequest;
use App\Http\Resources\ProjectCollection;
use App\Models\Project;
use App\Http\Resources\Project as ProjectResource;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    public function index(Request $request)
    {
        return new ProjectCollection(Project::all());
    }

    public function one(Project $project)
    {
        return new ProjectResource($project);
    }

    public function create(ProjectCreateRequest $request)
    {
        $values = $request->validated();

        return new ProjectResource(Project::create($values));
    }

    public function update(Project $project, ProjectUpdateRequest $request)
    {
        $values = $request->validated();

        $project->update($values);

        return new ProjectResource($project);
    }

    public function delete(Project $project)
    {
        $project->delete();
        return response([], 204);
    }
}
