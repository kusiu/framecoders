<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskCreateRequest;
use App\Http\Requests\TaskUpdateRequest;
use App\Http\Resources\Task as TaskResource;
use App\Http\Resources\TaskCollection;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    public function index(Request $request)
    {
        return new TaskCollection(Task::all());
    }

    public function one(Task $task)
    {
        return new TaskResource($task);
    }

    public function create(TaskCreateRequest $request)
    {
        $values = $request->validated();

        return new TaskResource(Task::create($values));
    }

    public function update(Task $task, TaskUpdateRequest $request)
    {
        $values = $request->validated();

        $task->update($values);

        return new TaskResource($task);
    }

    public function delete(Task $task)
    {
        $task->delete();
        return response([], 204);
    }
}
