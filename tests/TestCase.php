<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * If true, setup has run at least once.
     * @var boolean
     */
    protected static $setUpHasRunOnce = false;

    /**
     *
     */
    protected function setUp(): void
    {
        parent::setup();

        if (!static::$setUpHasRunOnce) {
            Artisan::call('config:cache --env=testing');
            Artisan::call('migrate:fresh');
            static::$setUpHasRunOnce = true;
        }
    }
}
