<?php

namespace Tests\Feature;

use App\Models\Project;
use App\Models\Task;
use Tests\TestCase;

class TaskTest extends TestCase
{
    private static Task $task;

    /** @test */
    public function create_task()
    {
        $project = Project::create([
            'name' => 'Project 1'
        ]);

        $input = [
            'title' => 'Task 1',
            'description' => 'Description 1',
            'project_id' => $project->id,
        ];

        $response = $this->post('/tasks', $input);
        static::$task = Task::find($response->getOriginalContent()->id);
        $response->assertCreated()->assertJsonFragment([
            'data' => [
                'id' => static::$task->id,
                'title' => 'Task 1',
                'description' => 'Description 1',
                'project_id' => $project->id,
            ]
        ]);
    }

    /** @test */
    public function get_task_list()
    {
        $response = $this->get('/tasks');
        $response->assertOk();
        $response->assertJsonStructure(['data']);
        $this->assertCount(1, $response->getOriginalContent());
    }

    /** @test */
    public function update_task()
    {
        $input = [
            'title' => 'Task 2',
        ];

        $response = $this->put(sprintf('/tasks/%d/', static::$task->id), $input);
        $response->assertOk()->assertJsonFragment(
            [
                'title' => 'Task 2',
            ]
        );
    }

    /** @test */
    public function get_one_task()
    {
        $response = $this->get(sprintf('/tasks/%d/', static::$task->id));
        $response->assertOk()->assertJsonFragment(
            [
                'id' => static::$task->id,
                'title' => 'Task 2',
                'description' => 'Description 1',
            ]
        );
    }

    /** @test */
    public function delete_a_task()
    {
        $response = $this->delete(sprintf('/tasks/%d/', static::$task->id));
        $response->assertStatus(204);

        $this->assertNull(Task::find(static::$task->id));
    }
}
