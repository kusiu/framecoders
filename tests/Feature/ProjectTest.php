<?php

namespace Tests\Feature;

use App\Models\Project;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    private static Project $project;

    /** @test */
    public function create_project()
    {
        $input = [
            'name' => 'Project 1',
        ];

        $response = $this->post('/projects', $input);
        static::$project = Project::find($response->getOriginalContent()->id);
        $response->assertCreated()->assertJsonFragment([
            'data' => [
                'id' => static::$project->id,
                'name' => 'Project 1',
            ]
        ]);
    }

    /** @test */
    public function get_project_list()
    {
        $response = $this->get('/projects');
        $response->assertOk();
        $response->assertJsonStructure(['data']);
        $this->assertCount(1, $response->getOriginalContent());
    }

    /** @test */
    public function update_project()
    {
        $input = [
            'name' => 'Project 2',
        ];

        $response = $this->put(sprintf('/projects/%d/', static::$project->id), $input);
        $response->assertOk()->assertJsonFragment(
            [
                'name' => 'Project 2',
            ]
        );
    }

    /** @test */
    public function get_one_project()
    {
        $response = $this->get(sprintf('/projects/%d/', static::$project->id));
        $response->assertOk()->assertJsonFragment(
            [
                'id' => static::$project->id,
                'name' => 'Project 2',
            ]
        );
    }

    /** @test */
    public function delete_a_project()
    {
        $response = $this->delete(sprintf('/projects/%d/', static::$project->id));
        $response->assertStatus(204);

        $this->assertNull(Project::find(static::$project->id));
    }
}
