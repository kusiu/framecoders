## Project CRUD

Task from Framecoders 

## Requirements
- PHP `(min 7.4)`
- Node
- npm

## Installation
1. `composer install`
2. `php artisan migrate --seed`
3. `npm install`
4. `php artisan serve`

## Testing
`php artisan test`
