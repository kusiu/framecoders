<?php

namespace Database\Seeders;

use App\Models\Project;
use App\Models\Task;
use Illuminate\Database\Seeder;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $project1 = Project::create([
            'name' => 'Project 1'
        ]);

        Task::create([
            'title' => "Project 1 Task 1",
            'description' => "Project 1 Description 1",
            'project_id' => $project1->id
        ]);

        $project2 = Project::create([
            'name' => 'Project 2'
        ]);

        Task::create([
            'title' => "Project 2 Task 1",
            'description' => "Project 2 Description 1",
            'project_id' => $project2->id
        ]);
    }
}
