import React, { Component } from "react";
import axios from 'axios';
import Table from 'react-bootstrap/Table';

export default class ProjectsListing extends Component {

    constructor(props) {
        super(props)
        this.state = {
            projects: []
        };
    }

    componentDidMount() {
        axios.get('projects')
            .then(res => {
                this.setState({
                    projects: res.data
                });
            })
            .catch((error) => {
                console.log(error);
            })
    }

    render() {
        return (<div className="table-wrapper">
            <Table striped bordered hover>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                
                </tbody>
            </Table>
        </div>);
    }
}
