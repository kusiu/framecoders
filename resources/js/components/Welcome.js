import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.css';

import ProjectsListing from "./ProjectsListing";

function Welcome() {
    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card text-center">
                        <div className="card-header"><h2>Projects Manager</h2></div>
                        <Router>
                            <div>
                                <nav>
                                    <ul>
                                        <li>
                                            <Link to="/">Home</Link>
                                        </li>
                                        <li>
                                            <Link to="/projects-listing">Projects Listing</Link>
                                        </li>
                                    </ul>
                                </nav>
                                <Switch>
                                    <Route path="/projects-listing">
                                        <ProjectsListing />
                                    </Route>
                                </Switch>
                            </div>
                        </Router>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Welcome;

// DOM element
if (document.getElementById('app')) {
    ReactDOM.render(<Welcome />, document.getElementById('app'));
}
